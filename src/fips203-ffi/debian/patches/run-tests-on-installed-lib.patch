From 810363512f11dfe19302367866905eeaa74d9a08 Mon Sep 17 00:00:00 2001
From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Thu, 6 Jun 2024 13:22:14 +0200
Subject: [PATCH] ffi: Adjust tests to be able to test a system-installed
 library

Forwarded: https://github.com/integritychain/fips203/pull/13

This lets me re-use the FFI test suite on GNU/Linux without rust or
cargo installed, while still letting test suite work during
development.
---
 ffi/Makefile   | 36 +++++++++++++++++++++++++++---------
 ffi/baseline.c |  2 +-
 2 files changed, 28 insertions(+), 10 deletions(-)

diff --git a/tests/Makefile b/tests/Makefile
index 8e3e25e..bcb8259 100644
--- a/tests/Makefile
+++ b/tests/Makefile
@@ -1,24 +1,42 @@
-SO_LOCATION = ../../target/debug
+#!/usr/bin/make -f
+
+# Usage: (cargo build && cd tests && make)
+
+# If the library and its development headers are installed system-wide,
+# run the tests with:
+#
+#  (cd tests && make AS_INSTALLED=true)
+
 SIZES = 512 768 1024
 FRAMES = encaps_key decaps_key ciphertext encaps decaps keygen
 # should derive SONAME somehow, e.g. from CARGO_PKG_VERSION_MAJOR
 SONAME = 0
 
+all: check
+
+# adjustments for testing the local debug or release build:
+ifneq ($(AS_INSTALLED),true)
+SO_LOCATIONS = $(foreach w,.. ../..,$(foreach x,release debug,$w/target/$x $w/target/*/$x))
+SO_LOCATION = $(dir $(firstword $(foreach d,$(SO_LOCATIONS),$(wildcard $d/libfips203.so))))
+$(SO_LOCATION)libfips203.so.$(SONAME): $(SO_LOCATION)libfips203.so
+	ln $< $@
+COMPILE_FLAGS = -L $(SO_LOCATION) -I..
+RUN_PREFIX = LD_LIBRARY_PATH=$(SO_LOCATION)
+ADDITIONAL_RUN_DEPENDS = $(SO_LOCATION)libfips203.so.$(SONAME)
+endif
+
 BASELINES=$(foreach sz, $(SIZES), baseline-$(sz))
 CHECKS=$(foreach sz, $(SIZES), runtest-$(sz))
 
 check: $(CHECKS)
 
-$(SO_LOCATION)/libfips203.so.$(SONAME): $(SO_LOCATION)/libfips203.so
-	ln -s $< $@
-
-runtest-%: baseline-% $(SO_LOCATION)/libfips203.so.$(SONAME)
-	LD_LIBRARY_PATH=$(SO_LOCATION) ./$<
+runtest-%: baseline-% $(ADDITIONAL_RUN_DEPENDS)
+	$(RUN_PREFIX) ./$<
 
 baseline-%: baseline.c ../fips203.h
-	$(CC) -o $@ -g -D MLKEM_size=$* $(foreach v, $(FRAMES),-D MLKEM_$(v)=ml_kem_$*_$(v)) -Werror -Wall -pedantic -L $(SO_LOCATION) $< -Wall -lfips203
+	$(CC) -o $@ -g -D MLKEM_size=$* $(foreach v, $(FRAMES),-D MLKEM_$(v)=ml_kem_$*_$(v)) -Werror -Wall -pedantic $< -Wall $(COMPILE_FLAGS) -lfips203
 
 clean:
-	rm -f $(BASELINES)
+	rm -f $(BASELINES) $(ADDITIONAL_RUN_DEPENDS)
 
-.PHONY: clean check
+.PHONY: clean check all
diff --git a/tests/baseline.c b/tests/baseline.c
index 2da4d0f..611ee0c 100644
--- a/tests/baseline.c
+++ b/tests/baseline.c
@@ -1,5 +1,5 @@
 #include <stdio.h>
-#include "../fips203.h"
+#include <fips203.h>
 
 int main(int argc, const char **argv) {
   MLKEM_encaps_key encaps;
-- 
2.43.0

