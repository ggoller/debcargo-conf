Description: Treat ASCII control characters as zero width
 to deal with unicode-width 0.1.13 behavior change, which the upstream states
 is not for terminal use. Here we naively skip them.
 Based on the prettytable-rs patch by Peter Green (plugwash).
Last-Update: 2024-09-04
---
This patch header follows DEP-3: http://dep.debian.net/deps/dep3/
--- a/src/util/string.rs
+++ b/src/util/string.rs
@@ -7,2 +7,24 @@
 /// Returns string width and count lines of a string. It's a combination of [`string_width_multiline`] and [`count_lines`].
+
+trait UnicodeWidthChar {
+	fn width(self) -> Option<usize>;
+}
+
+trait UnicodeWidthStr {
+	fn width(&self) -> usize;
+}
+
+impl UnicodeWidthChar for char {
+	fn width(self) -> Option<usize> {
+		if self as u32 <= 0x1F { return Some(0) }
+		unicode_width::UnicodeWidthChar::width(self)
+	}
+}
+
+impl UnicodeWidthStr for str {
+	fn width(&self) -> usize {
+		self.split(|c| c as u32 <= 0x1F).map(unicode_width::UnicodeWidthStr::width).sum()
+	}
+}
+
 #[cfg(feature = "std")]
@@ -15,3 +37,3 @@
             } else {
-                let w = unicode_width::UnicodeWidthChar::width(c).unwrap_or(0);
+                let w = UnicodeWidthChar::width(c).unwrap_or(0);
                 (lines, acc + w, max)
@@ -35,3 +57,3 @@
     {
-        unicode_width::UnicodeWidthStr::width(text)
+        UnicodeWidthStr::width(text)
     }
@@ -46,3 +68,3 @@
             .map(|e| &text[e.start()..e.end()])
-            .map(unicode_width::UnicodeWidthStr::width)
+            .map(UnicodeWidthStr::width)
             .sum()
@@ -56,3 +78,3 @@
         text.lines()
-            .map(unicode_width::UnicodeWidthStr::width)
+            .map(UnicodeWidthStr::width)
             .max()
