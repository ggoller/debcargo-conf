Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nucleo
Upstream-Contact: Pascal Kuthe <pascalkuthe@pm.me>
Source: https://github.com/helix-editor/nucleo

Files: *
Copyright:
 2023-2024 Pascal Kuthe <pascalkuthe@pm.me>
 2024 slekup <opensource@slekup.com>
 2023-2024 blixen <h-k-81@hotmail.com>
 2023 Michael Davis <mcarsondavis@gmail.com>
 2023 a-kenji <aks.kenji@protonmail.com>
 2023 Knut Aldrin <knut@aldrin.cc>
 2023 Poliorcetics
 2023 Riccardo Mazzarini <me@noib3.dev>
 2023 Gabriel Dinner-David
 2023 Tudyx
 2023 Ivan Enderlin <ivan@mnt.io>
License: MPL-2.0

Files: src/boxcar.rs
Copyright: 2022 Ibraheem Ahmed
License: MIT

Files: src/par_sort.rs
Copyright: 2010 The Rust Project Developers
License: MIT or Apache-2.0

Files: debian/*
Copyright:
 2024 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2024 Federico Ceratto <federico@debian.org>
License: MPL-2.0

License: Apache-2.0
 Debian systems provide the Apache 2.0 license in
 /usr/share/common-licenses/Apache-2.0

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: MPL-2.0
 Debian systems provide the MPL 2.0 in /usr/share/common-licenses/MPL-2.0
