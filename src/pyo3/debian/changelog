rust-pyo3 (0.22.2-4) unstable; urgency=medium

  * Team upload.
  * Package pyo3 0.22.2 from crates.io using debcargo 2.6.1
  * Add breaks on old versions of rust-debian-control and rust-pyo3-filelike.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 06 Aug 2024 00:29:32 +0000

rust-pyo3 (0.22.2-3) unstable; urgency=medium

  * Team upload.
  * Package pyo3 0.22.2 from crates.io using debcargo 2.6.1
  * Add breaks on old versions of rust-debian-analyzer and rust-debversion.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 04 Aug 2024 18:15:07 +0000

rust-pyo3 (0.22.2-2) unstable; urgency=medium

  * Team upload.
  * Package pyo3 0.22.2 from crates.io using debcargo 2.6.1
  * Restore riscv changes in debian/tests/control which were lost
    in 0.22.2 update.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 03 Aug 2024 14:57:55 +0000

rust-pyo3 (0.22.2-1) unstable; urgency=medium

  * Package pyo3 0.22.2 from crates.io using debcargo 2.6.1
   + Drop patch use-mutex-if-atomicu64-unavailable: fixed upstream.
   + Drop patch newer-widestring: fixed upstream.
   + Drop patch either-frompyobject: no longer necessary.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 30 Jul 2024 00:23:03 +0100

rust-pyo3 (0.20.2-4) unstable; urgency=medium

  * Team upload.
  * Package pyo3 0.20.2 from crates.io using debcargo 2.6.1
  * Stop marking abi3-py312 test as broken, debian now has python 3.12 as
    default
  * Mark "full" test as not broken, it was implicitly marked as broken
    by stuff it depends on, but it's not actually broken.
  * Adjust autopkgtests to hopefully avoid "global timeout" issues on riscv
    + Stop running broken tests.
    + Skip many test configurations on risv64.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 13 Jul 2024 16:20:35 +0000

rust-pyo3 (0.20.2-3) unstable; urgency=medium

  * Team upload.
  * Package pyo3 0.20.2 from crates.io using debcargo 2.6.1
  * Relax memoffset dependency.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 08 Jul 2024 04:55:53 +0000

rust-pyo3 (0.20.2-2) unstable; urgency=medium

  * Team upload.
  * Package pyo3 0.20.2 from crates.io using debcargo 2.6.1
  * Add patch to use a Mutex on architectures where AtomicU64 is not available
  * Mark tests for the "abi3-py312" feature as "broken" since the default
    python version in Debian is still 3.11.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 21 Jan 2024 20:58:20 +0000

rust-pyo3 (0.20.2-1) unstable; urgency=medium

  * Team upload.
  [ Antonio Russo ]
  * Package pyo3 0.20.2 from crates.io using debcargo 2.6.1

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sat, 20 Jan 2024 22:23:10 +0000

rust-pyo3 (0.19.2-1) unstable; urgency=medium

  * Team upload.
  * Package pyo3 0.19.2 from crates.io using debcargo 2.6.0
  * Remove upper limit from indexmap dependency.
  * Add upstream patch to make tests pass with new chrono. (Closes: #1055120)

  [ Jelmer Vernooĳ ]
  * Drop obsolete patch newer-indexmap
  * Update copyright file.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 09 Nov 2023 20:25:29 +0000

rust-pyo3 (0.19.0-3) unstable; urgency=medium

  * Team upload.
  * Add debian/patches/newer-widestring to bump widestring dependency.
  * Drop debian/patches/older-widestring.
  * Refresh debian/patches/newer-indexmap.

  [ Peter Michael Green ]
  * Reduce the number of tests marked as broken.

 -- Emanuele Rocca <ema@debian.org>  Wed, 05 Jul 2023 16:05:47 +0200

rust-pyo3 (0.19.0-2) unstable; urgency=medium

  * Team upload.
  * Package pyo3 0.19.0 from crates.io using debcargo 2.6.0
  * Bump dependencies on unindent and indoc.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 25 Jun 2023 01:54:19 +0000

rust-pyo3 (0.19.0-1) unstable; urgency=medium

  * Package pyo3 0.19.0 from crates.io using debcargo 2.6.0

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 11 Jun 2023 14:12:18 +0100

rust-pyo3 (0.18.3-1) experimental; urgency=medium

  * Package pyo3 0.18.3 from crates.io using debcargo 2.6.0

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 30 May 2023 16:40:03 +0100

rust-pyo3 (0.17.3-1) unstable; urgency=medium

  * Package pyo3 0.17.3 from crates.io using debcargo 2.6.0 (Closes: #1025685)

  [ Claudius Heine ]
  * Team upload.
  * Source upload
  * Mark some autopkgtests as broken, because they depend on other features,
    which are missing. (Closes: #1021637)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 10 Dec 2022 20:54:38 +0000

rust-pyo3 (0.16.5-2) unstable; urgency=medium

  * Team upload.
  * Source upload
  * Package pyo3 0.16.5 from crates.io using debcargo 2.6.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 21 Nov 2022 10:26:35 +0100

rust-pyo3 (0.16.5-1) unstable; urgency=medium

  * Source upload
  * Package pyo3 0.16.5 from crates.io using debcargo 2.5.0

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Sep 2022 20:46:11 +0100
