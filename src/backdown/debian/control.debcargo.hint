Source: rust-backdown
Section: utils
Priority: optional
Build-Depends: debhelper (>= 12),
 dh-cargo (>= 25),
 cargo:native,
 rustc:native (>= 1.59),
 libstd-rust-dev,
 librust-anyhow-1+default-dev (>= 1.0.49-~~),
 librust-argh-0.1+default-dev (>= 0.1.4-~~),
 librust-blake3-1+default-dev (>= 1.4-~~),
 librust-chrono-0.4+default-dev,
 librust-cli-log-2+default-dev,
 librust-crossbeam-0.8+default-dev,
 librust-crossterm-0.27+default-dev,
 librust-file-size-1+default-dev,
 librust-fnv-1+default-dev (>= 1.0.7-~~),
 librust-lazy-regex-2+default-dev (>= 2.2.2-~~),
 librust-phf-0.11+default-dev,
 librust-phf-0.11+macros-dev,
 librust-rayon-1+default-dev (>= 1.3-~~),
 librust-serde-1+default-dev,
 librust-serde-json-1+default-dev,
 librust-termimad-0.29+default-dev
Maintainer: Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
Uploaders:
 Blair Noctis <ncts@debian.org>
Standards-Version: 4.6.2
Vcs-Git: https://salsa.debian.org/rust-team/debcargo-conf.git [src/backdown]
Vcs-Browser: https://salsa.debian.org/rust-team/debcargo-conf/tree/master/src/backdown
X-Cargo-Crate: backdown
Rules-Requires-Root: no

Package: librust-backdown-dev
Architecture: any
Multi-Arch: same
Depends:
 ${misc:Depends},
 librust-anyhow-1+default-dev (>= 1.0.49-~~),
 librust-argh-0.1+default-dev (>= 0.1.4-~~),
 librust-blake3-1+default-dev (>= 1.4-~~),
 librust-chrono-0.4+default-dev,
 librust-cli-log-2+default-dev,
 librust-crossbeam-0.8+default-dev,
 librust-crossterm-0.27+default-dev,
 librust-file-size-1+default-dev,
 librust-fnv-1+default-dev (>= 1.0.7-~~),
 librust-lazy-regex-2+default-dev (>= 2.2.2-~~),
 librust-phf-0.11+default-dev,
 librust-phf-0.11+macros-dev,
 librust-rayon-1+default-dev (>= 1.3-~~),
 librust-serde-1+default-dev,
 librust-serde-json-1+default-dev,
 librust-termimad-0.29+default-dev
Provides:
 librust-backdown+default-dev (= ${binary:Version}),
 librust-backdown-1-dev (= ${binary:Version}),
 librust-backdown-1+default-dev (= ${binary:Version}),
 librust-backdown-1.1-dev (= ${binary:Version}),
 librust-backdown-1.1+default-dev (= ${binary:Version}),
 librust-backdown-1.1.1-dev (= ${binary:Version}),
 librust-backdown-1.1.1+default-dev (= ${binary:Version})
Description: Smart CLI for removing thousands of duplicates on your disks - Rust source code
 Source code for Debianized Rust crate "backdown"

Package: backdown
Architecture: any
Multi-Arch: allowed
Section: utils
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
 ${cargo:Depends}
Recommends:
 ${cargo:Recommends}
Suggests:
 ${cargo:Suggests}
Provides:
 ${cargo:Provides}
Built-Using: ${cargo:Built-Using}
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: helps you safely and ergonomically remove duplicate files
 Based on author's observation of frequent patterns of duplicate build-up with
 time, especially images and other media files, backdown asks some questions
 about the duplicates in a way the user can easily decide which to keep. The
 list is staged until the end for the user to confirm removal, or it can be
 optionally exported as JSON for further inspection. It also has the option to
 replace files to be removed with links pointing to the kept ones.
